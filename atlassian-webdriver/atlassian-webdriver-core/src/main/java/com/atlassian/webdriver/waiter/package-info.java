/**
 * <p/>
 * WebDriver Waiter utility for performing timed queries on
 * elements and executing timed functions
 *
 * <p/>
 * WARNING: This API is still experimental and may be changed between versions.
 *
 * <p/>
 * For timed queries on {@code WebDriver} elements, consider using utilities in the
 * {@code com.atlassian.webdriver.utils.element} package. For enhanced HTML element representation (incl. timed
 * queries support, consider using the {@code atlassian-pageobjects-elements} module.
 *
 * @since 2.1.0
 * @see com.atlassian.webdriver.utils.element.WebDriverPoller
 */
@ExperimentalApi
package com.atlassian.webdriver.waiter;

import com.atlassian.annotations.ExperimentalApi;